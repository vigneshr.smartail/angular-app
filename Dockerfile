# FROM nginx:1.17.4-alpine
# COPY /dist/my-app /usr/share/nginx/html

# # base image
FROM node:latest as node

# # set working directory
WORKDIR /usr/local/app
COPY package.json package-lock.json ./
COPY ./ /usr/local/my-app/

# # install and cache app dependencies
RUN npm install
COPY . .
RUN npm run build my-app
# RUN ng build my-app

FROM nginx:1.17.4-alpine
RUN rm -rf /usr/share/nginx/html/* && rm -rf /etc/nginx/nginx.conf
COPY ./nginx.conf /etc/nginx/nginx.conf 
COPY --from=node /usr/local/app/dist /usr/share/nginx/html