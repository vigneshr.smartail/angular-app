import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http:HttpClient, private router:Router) { }

  ngOnInit(): void {
  }

  loginClick(data:any) {
    console.warn(data);
    this.http.post('http://localhost:8080/student',data).
    subscribe((data:any) => {
        console.warn(data);
        if(data==true){
          this.router.navigate(["/dashboard"]);
        }
      }
    );
  }  

}
