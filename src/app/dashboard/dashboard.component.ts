import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  students: any;
  student: any;
  student1: any;
  roll: any;

  constructor( private http:HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('http://localhost:8080/getStudents')
      .subscribe((result) => {
        this.students = result; // result is an array of students //displaying the students in the grid
        console.log(this.students)
      })
  }

  getStudents() {
    this.http.get('http://localhost:8080/getStudents')
    .subscribe((result) => {
      this.students = result; // result is an array of students //displaying the students in the grid
      console.log(this.students)
    })

  }

  getStudent(data): void {
    console.log("rollno")
    console.log(data.rollno,"rollno")
    this.http.get('http://localhost:8080/getStudent?rollno='+data.rollno)
    .subscribe((result) => {
      this.student = result; // result is an array of students //displaying the students in the grid
      console.log(this.student.name)
      console.log(this.student)      
    }
    );
    this.student = null;
  }

  getStudent1(data): void {
    console.log("rollno")
    console.log(data.rollno,"rollno")
    this.http.get('http://localhost:8080/getStudent?rollno='+data.rollno)
    .subscribe((result) => {
      this.student1 = result; // result is an array of students //displaying the students in the grid
      console.log(this.student1.name)
      console.log(this.student1)
    }
    );
  }
  
  addStudent(data): void {
    console.log(data)
    if(data.rollno=="")
      data.rollno=this.student1.rollno;
    if(data.name=="")
      data.name=this.student1.name;
    if(data.age=="")
      data.age=this.student1.age;
    if(data.phn=="")
      data.phn=this.student1.phn;

    console.log(data)
    this.http.put('http://localhost:8080/student',data,{ responseType: 'text' })
    .subscribe((result) => {
      console.log(result, "student added")
        if("student updated"==result){
          alert("Student updated successfully");
        }
        else if("student added"==result)
          alert("Student added successfully");
          // window.location.reload();
      }
    );
    this.student1=null;
  }

  delete(rollno): void {
    console.log(rollno)
    this.http.delete('http://localhost:8080/student?rollno='+rollno,{ responseType: 'text' })
    .subscribe((result) => {
      console.log(result)
        if("deleted successfully"==result){
          alert("Student deleted successfully");
          this.getStudents();
        }
        else
          alert("Something went wrong!!!");
      } 
    );
  }
}